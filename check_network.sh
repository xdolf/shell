#!/bin/bash

#file: check_network.sh
#intro: check network by ping, restart network if necessary
#usage: 
#
#
#by dolf
#2019.8.8

echo -e "\n-------------------------------------------"
date +"%Y-%m-%d %H:%M:%S"

basepath=$(cd `dirname $0`;pwd)

result=$(ping -4 -c 3 -i 1 www.sysu.edu.cn 2>&1)
code=$?
echo -e "ping result: code=[${code}]\n'${result}'\n"

if [ $code -gt 0 ];then
	result=$(ifconfig)
	echo -e "ifconfig:\n'${result}'\n"
	result=$(/etc/init.d/networking restart)
	code=$?
	echo -e "restart network: code=[${code}]\n'${result}'\n"
fi
