#/bin/bash
#restart transmission-daemon when data get crash, especially for nfs/samba
#must run with user root

basepath=$(cd `dirname $0`;pwd)
#echo $basepath

d=$(date +"%Y-%m-%d %H:%M:%S")
echo "[$d]"

#username and password for transmission-daemon
username='root'
password='admin'

#power down of router detect
power=`${basepath}/ether_plug_detect.sh`
power=""
echo power:[${power}]
[ $power ] && [ $power == "no" ] && echo ether link not detected, maybe power down! \
  && exit 0

#get error info
result=$(transmission-remote --auth ${username}:${password} -t all --start)
result=$(transmission-remote --auth ${username}:${password} -t all -i | grep 'Error: No data found')
#echo -e "result:\n[${result}]"

#number of errors
n=$(echo -n "$result" | wc -l)
echo n:$n

[ $n ] && [ $n -gt 4 ] && echo error! restarting trans ... \
  && /etc/init.d/transmission-daemon restart

