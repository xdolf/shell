#detect if wire is plugged in, 
#output: yes or no, return 1 other wise
#ethtool can run without root, while mii-tool must run with root
result=$(ethtool eth0 2>/dev/null | awk '/Link detected:/{print $3}')
echo $result
[ ! -n $result ] && return 1
