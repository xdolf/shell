
usage:
run the following command
./set_screen_bash.sh
and the $PS1 will be set to display screen status with 'S' after the number of shell like:
	pi@raspberrypi-dolf(1S)~/shell/screen[21:13:42]$

to start screen at start of shell, put the following command to ~/.bashrc or Xshell start script(recommended):
	which screen >> /dev/null 2>&1  && [ -z "$STY" ] && screen -Rd "work"
