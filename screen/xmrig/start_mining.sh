path=$1
file=$2

#start gpu test
name="cpu"
#kill screen if exist
screen -S $name -X quit >/dev/null 2>&1
#create screen in deteach mode
screen -L -t $name -dmS $name
#send command to screen 
cmd="cd $path && ./$file"
screen -x -p 0 -S $name -X stuff "$cmd\\n"

