#monitor process Jupyter Hub, 
#if exist, kill screen, 
#if not exist, start screen

#screen parameters
name='cpu'
path='/home/dolf/software/xmrig/build/xmrig/bin'
cmd='./cputest'

echo -e "\n-------------------------------------------"
date +"%Y-%m-%d %H:%M:%S"

header=$(top -bn 1 | awk '/^  PID USER/{print $0}');
cpu=$(top -bn 1 | awk '/^[0-9]* dolf.*ZMQbg/{if($9>50)print $0}');
#Jupyter Hub process cpu usage>50%: kill screen
if [ "a$cpu" != "a" ];then
	echo $header
	echo $cpu
	#kill screen if exist
	su - root -c "screen -S $name -X quit" >/dev/null 2>&1
	echo "Screen '$name' killed!"
else
	result_screen=$(screen -ls | grep $name)
	#screen not exist: start it
	if [ "a$result_screen" == "a" ];then
		#create screen in deteach mode
		su - root -c "screen -L -t $name -dmS $name"
		#send command to screen 
		su - root -c "screen -s bash -x -p 0 -S $name -X stuff \"cd $path && $cmd\\n\""
		echo "Screen '$name' started!"
	else
		echo "Screen '$name' is running!"
	fi
fi
