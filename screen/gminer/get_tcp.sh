ps=$(ps -ef | grep gputest | grep watchdog_child_process | awk '{print $2," ",$17}'); 
pid=$(echo "$ps" | tail -n 1 | awk '{print $1}'); 
lsof -p $pid -nP | grep --color TCP
