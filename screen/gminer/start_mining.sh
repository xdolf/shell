path=$1
file=$2

#start gpu test
name="gpu"
#kill screen if exist
su - dolf -c "screen -S $name -X quit" >/dev/null 2>&1
#create screen in deteach mode
su - dolf -c "screen -L -t $name -dmS $name"
#send command to screen 
cmd="cd $path && ./$file"
su - dolf -c "screen -x -p 0 -S $name -X stuff \"$cmd\\n\""


