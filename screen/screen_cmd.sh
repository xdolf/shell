
name=$1
path=$2
cmd=$3
#kill screen if exist
screen -S $name -X quit >/dev/null 2>&1
#create screen in deteach mode
screen -L -t $name -dmS $name
#send command to screen 
screen -s bash -x -p 0 -S $name -X stuff "cd $path && $cmd\\n"
