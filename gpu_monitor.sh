#monitor temperature of GPUs

echo ===================================
d=$(date +"%Y-%m-%d %H:%M:%S")
echo $d

t=$(nvidia-smi -q -d TEMPERATURE | awk '/GPU Current Temp/{print $5}');
echo $t;
n=$(for i in $t;do [ -n "$i" ] && [ "$i" -gt 60 ] && echo $i;done | wc -l);
echo $n

#usage of GPUs>4
if [ "$n" -gt 4 ];then
    nvidia-smi | sed 's/[^[:print:]]//' \
        | mail -s "Report from $(hostname): $n GPUs temperature grater than 60 degree!" xieshundao@qq.com
#          -r "dolf@$(hostname).local" xieshundao@qq.com
    echo mail sent
fi

