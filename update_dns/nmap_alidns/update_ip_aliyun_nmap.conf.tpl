#!/bin/bash

#template config file
#please remove '.tpl' from filename after edit
#dns as: ${name}.${domain}
domain="your-domain.com"
name="www1"
#ip range to be scan
ips='172.18.216.0/22'
#mac of host: upper case hex
mac='24:5E:BE:37:A8:50'
