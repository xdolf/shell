#!/bin/bash

basepath=$(cd `dirname $0`;pwd)
#source ${basepath}/ips_mac.conf.tpl
#if [ -n "$1" ];then
#   [ ! -f $1 ] && echo "Usage: ./get_ip_nmap.sh [config-file]" && exit 1
#   source $1
#else
#   conf_file=${basepath}/ips_mac.conf
#   [ ! -f $conf_file ] && echo "default config file $conf_file not exist!" && exit 1
#   source $conf_file
#fi

if [ -z "$1" -o -z "$2" ];then
   echo "Usage: ./get_ip_nmap.sh ip_range mac" && exit 1
fi

ips=$(echo $1 | awk '{match($0,/(([0-9]{1,3}\.){3}[0-9]{1,3}\/[0-9]{1,2})/,a);print a[1]}')
[ -z "$ips" ] && echo "Error: wrong ip range format. Example: 123.456.789.0/32" && exit 1
mac=$(echo $2 | awk '{match($0,/(([0-9A-F]{2}:){5}[0-9A-F]{2})/,a);print a[1]}')
[ -z "$mac" ] && echo "Error: wrong mac format. Example: A1:B2:C3:D4:E5:F6" && exit 2

nmap -sP $ips | grep -B 2 $mac | awk '/^Nmap scan report for /{print $5}'
