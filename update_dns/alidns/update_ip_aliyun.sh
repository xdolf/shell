#!/bin/bash

#update or add aliyun dns(A record only)
#need config 'Aliyun/AliyunClient.class.php' and install php5.3+ and php_curl
#usage: 
#update dns:  ./update_ip_aliyun.sh $conf_file
#add new dns: ./update_ip_aliyun.sh $conf_file yes
#by dolf
#2018.6.6

#add new name 
add="no"
[ -n "$2" ] && add=$2

echo -e "\n-------------------------------------------"
date +"%Y-%m-%d %H:%M:%S"

basepath=$(cd `dirname $0`;pwd)
source ${basepath}/update_ip_aliyun.conf.tpl
if [ -n "$1" ];then
   [ ! -f $1 ] && echo "Usage: ./update_ip_aliyun.sh [config-file] [yes(add dns)]" && exit 1
   source $1
else
   conf_file=${basepath}/update_ip_aliyun.conf
   [ ! -f $conf_file ] && echo "default config file $conf_file not exist!" && exit 1
   source $conf_file
fi

#get ip address of $interface and store in varialbe ip
[ ! -f ${basepath}/get_ip.sh ] && cp ${basepath}/get_ip.sh.tpl ${basepath}/get_ip.sh
for i in $interface;do
    IP=$(${basepath}/get_ip.sh ${i})
    #echo "${i}:'${IP}'"
    [ $IP ] && break;
done
    echo "IP:'${IP}'"
[ -z "$IP" ] && echo "null IP!" && exit 1

echo "Querying '${name}' from '${domain}' ..."
result=$(php ${basepath}/flc -list $domain 1 500 | grep " $name " | head -n 1)
echo "result:'$result'"
[ -z "$result" ] && [ $add ==  "yes" ] && echo "add new:${name}.${domain}" && php ${basepath}/flc -add $domain $name $IP && exit 0
[ -z "$result" ] && echo "name $name not exist!" && exit 1

id=$(echo $result | awk '{print $1}')
[ -z "$id" ] && echo "fail to get id!" && exit 1
echo "id:'$id'"

old_ip=$(echo $result | awk '{print $4}')
echo "old_ip:'$old_ip'"

[ "i$IP" == "i$old_ip" ] && echo "no change" && exit 0

php ${basepath}/flc -update $id $name $IP
echo -ne "\n"

