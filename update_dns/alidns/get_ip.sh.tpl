#!/bin/bash

#get ip address of interface
#usage: 
#./get_ip.sh interface
#by dolf
#2020.3.1

interface="eth0"
[ -n "$1" ] && interface=$2

#get ip address of $interface and store in varialbe ip
for i in $interface;do
    IP=$(/sbin/ifconfig ${i} |grep "inet addr" |cut -d: -f2 |cut -d" " -f1)
    #for centos 7
    #IP=$(/sbin/ifconfig ${i} 2>/dev/null | awk '/inet /{print $2}')
    #echo "${i}:'${IP}'"
    [ $IP ] && break;
done
    echo -ne ${IP}
[ -z "$IP" ] && exit 1

