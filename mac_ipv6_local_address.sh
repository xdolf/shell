#!/bin/bash

#file: .sh
#intro: set ipv6 link local address according to mac address
#usage: 
#
#
#by dolf
#2019.8.8

echo -e "\n-------------------------------------------"
date +"%Y-%m-%d %H:%M:%S"

basepath=$(cd `dirname $0`;pwd)
#echo "basepath:$basepath"
if [ "a$1" == "a" ];then
	echo "Usage: $0 interface"
	interface='eth0'
else
	interface=$1
fi

#RFC 4862 link local address
ip link set $interface addrgenmode eui64
#disable ipv6
sysctl -w net.ipv6.conf.all.disable_ipv6=1
sysctl -w net.ipv6.conf.default.disable_ipv6=1
#re-enable ipv6
sysctl -w net.ipv6.conf.all.disable_ipv6=0
sysctl -w net.ipv6.conf.default.disable_ipv6=0
