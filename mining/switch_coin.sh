#switch coin to mine according to 24 hour profit
#coin name
#names=("grin32" "grin29" "beam" "eth")
#hashrate
#rates=( 6.48      57.6    224    456)

echo -e "\n-------------------------------------------"
date +"%Y-%m-%d %H:%M:%S"

basepath=$(cd `dirname $0`;pwd)
source ${basepath}/coins.conf.tpl
conf_file=${basepath}/coins.conf
[ -f $conf_file ] && echo "source default config file: $conf_file" && source $conf_file

max_profit=0
max_idx=0
for ((i=0;i<${#names[*]};i++));do
	echo -n "${names[$i]} (hashrate=${rates[$i]}): "
	url="https://2cryptocalc.com/coin/ajax/en/pool/${names[$i]}/${rates[$i]}"
	#echo $url
	profit=$(curl -s $url | jq .h.usd)
	echo "\$$profit"
	profit_int=$(echo $profit | awk '{printf "%d",100*$0}')
	#echo "$profit_int"
	if [ $profit_int -gt $max_profit ];then
		max_profit=$profit_int
		max_idx=$i
	fi
done
max_coin=${names[$max_idx]}
echo "max_coin: $max_coin"

#start gpu test
name="gpu"
dir='/home/dolf/software/test/'
#cmd="./start_beam.sh"
[ -f ${dir}/last_coin.txt ] && last_coin=$(cat ${dir}/last_coin.txt)
if [ "a$max_coin" != "a$last_coin" ];then
	cmd="./start_${max_coin}.sh"
	/home/dolf/shell/screen/screen_cmd.sh $name $dir $cmd
	echo $max_coin > ${dir}/last_coin.txt
	echo "Switch from $last_coin to $max_coin"
fi

