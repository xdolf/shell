#!/bin/bash

#file: template.sh
#intro: this is a template for bash shell
#usage: 
#
#
#by dolf
#2019.8.8

#date
#echo -e "\n-------------------------------------------"
d=$(date +"%Y-%m-%d %H:%M:%S")
echo [${d}]

#path of the shell
basepath=$(cd `dirname $0`;pwd)
echo "basepath: $basepath"

#test empty string
str=
[ $str ] || echo \$str is null
