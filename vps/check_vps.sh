#by dolf
#create @ 2019.11.11
#modify @ 

#!/bin/bash

basepath=$(cd `dirname $0`;pwd)
logfile="${basepath}/check_vps.log"

d=$(date +"%Y-%m-%d %H:%M:%S")

echo $d | tee -a $logfile
echo =================================  | tee -a $logfile

url="https://ray.xdolf.tk"
result=$(curl -ksI -w "%{http_code}" $url | sed -n '1p;$p')
code=$(echo "$result" | sed -n '$p')
result=$(echo "$result" | sed -n '1p')

echo "Http return : $result" | tee -a $logfile

[ "c$code" != "c200" ] && cat $logfile \
      | sed -n "/$d/,\$p" | sed 's/[^[:print:]]//' \
		| mail -s "Error(code:${code}) from vps $url" \
		 -r "dolf@$(hostname).local" xieshundao@qq.com

