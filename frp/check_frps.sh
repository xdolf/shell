#!/bin/bash

#file: check_frps.sh
#intro: check screen frp, start it if not exist
#usage: 
#
#
#by dolf
#2021.1.30

#date
#echo -e "\n-------------------------------------------"
d=$(date +"%Y-%m-%d %H:%M:%S")
echo -n "[${d}] "

#path of the shell
basepath=$(cd `dirname $0`;pwd)
#echo "basepath: $basepath"

#find screen frp
str=$(screen -ls| grep '.frp')
if [ "s$str" == "s" ];then
	echo -n "Screen 'frp' not found! restart frps... "
	$basepath/../screen/screen_cmd.sh frp ~ '/usr/bin/frps -c /etc/frp/frps.ini'
fi
echo "OK"
