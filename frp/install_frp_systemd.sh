flag_systemd=$(which systemctl)
if [ "a$flag_systemd" != "a" ];then
	echo "Systemd found: $flag_systemd"
else
	echo "Systemd NOT found! Service script will not be installed."
fi
export FRP_VERSION=0.34.3
echo "Installing frp $FRP_VERSION ..."
sudo mkdir -p /etc/frp
wget "https://github.com/fatedier/frp/releases/download/v${FRP_VERSION}/frp_${FRP_VERSION}_linux_amd64.tar.gz"
tar xzvf frp_${FRP_VERSION}_linux_amd64.tar.gz
cd  frp_${FRP_VERSION}_linux_amd64/
sudo cp frps.ini /etc/frp/frps.ini
sudo cp frps /usr/bin/frps

flag_frps=$(which frps)
if [ "a$flag_frps" != "a" ];then
	echo " OK"
else
	echo " Failed!"
	exit 1
fi
if [ "a$flag_systemd" != "a" ];then
	echo -n "Installing frps.service to /lib/systemd/system/ ..."
	sudo cp systemd/frps.service /lib/systemd/system/
	sudo systemctl daemon-reload
	sudo systemctl enable frps
	sudo systemctl start frps
	sudo systemctl status frps
	echo " OK"
fi
