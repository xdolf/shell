#!/bin/bash

#file: check_wifi.sh
#intro: check wifi conection by network manager, restart connection if necessary
#usage: 
#
#
#by dolf
#2019.10.29

echo -e "\n-------------------------------------------"
date +"%Y-%m-%d %H:%M:%S"

basepath=$(cd `dirname $0`;pwd)
interface='wlan0'

status=$(nmcli device status | awk '/^'$interface'/{print $3}')
[ "d$status" == "ddisconnected" ] && echo "reconnectting $interface ..." && nmcli d connect $interface
