#Monitor kvm virtual machines, restart the client machine(s) if they get stuck
#By dolf, 2019.6.18
#Crontab job command line as follow:
#  m   h  dom mon dow   command
# */6  *   *   *   *    sh /root/shell/pve/monitor-kvm.sh >> /root/crontab.log 2>&1

#/bin/bash
basepath=$(cd `dirname $0`;pwd)
#echo $basepath

d=$(date +"%Y-%m-%d %H:%M:%S")
echo "[$d]"

#vmid file
echo "" > ${basepath}/vmid.txt
touch ${basepath}/vmid.old.txt

#Get list of client machines
vms=$(/usr/sbin/qm list)

#Find out the client(s) with CPU usage grater than 90%
#$9:CPU%, $1:PID
pids_usages=$(top -bn 1 | grep ' kvm$' | awk '{ if($9>90) print $1,$9}')

#reset the stuck client(s) if detected to be stuck twice in succession
echo "$pids_usages" | while read line;
do
	pid=$(echo $line | awk '{print $1}')
	usage=$(echo $line | awk '{print int($2)}')
        [ -z "$pid" ] && continue
        echo " PID:$pid";
        echo " Usage:$usage";

	#Get vmid according to PID
        vmid=$(echo "$vms" | grep $pid | awk '{if($3=="running") print $1}');
        echo "  VMID:$vmid"
	
	#Get cpu cores
	n_cores=$(qm config $vmid | awk '/cores: /{print $2}')
	echo "  cores:$n_cores"

	#Not every core usage > 95%
	if [ $((n_cores*95>usage)) ];then
		echo "  continue"
		continue
	fi

	#Get the privious check result
        old=$(cat ${basepath}/vmid.old.txt | grep $vmid )
        echo "  old:$old"

        if [ "a$old" != "a" ];then
		#second time stuck, reset
                echo "  reset ..."
                /usr/sbin/qm reset $vmid
        else
		#First time stuck, wait for next check
                echo "  quit"
                echo $vmid >> ${basepath}/vmid.txt
        fi
done
mv ${basepath}/vmid.txt ${basepath}/vmid.old.txt

