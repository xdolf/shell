
#Update /etc/hosts for name of $names according to result of domain name $domain
#By dolf, 2019.6.18

#/bin/bash
names="pve1 pve2 pve3"
domain="idart.site"
file="/etc/hosts"
for name in $names;do
	echo name:[${name}]

	#new ip
	ip1=$(host ${name}.$domain | awk '/'$domain' has address/{print $4}'| awk '/[0-9]*\.[0-9]*\.[0-9]*/{print $0}')
	[ "1$ip1" != "1" ] && echo ip of ${name}.idart.site:[${ip1}]

	#old ip
	ip2=$(cat $file | awk '/[0-9]*\.[0-9]*\.[0-9]*[ \t]*'$name'$/{print $1}')
	[ "1$ip2" != "1" ] && echo ip of $name in ${file}:[${ip2}]

	#ip change: new not null and new != old
	if [ "1$ip1" != "1" ] && [ "$ip1" != "$ip2" ];then
		#old ip exist: change it
		[ "1$ip2" != "1" ] && sed -i 's/'$ip2'\([ \t]*'$name'$\)/'$ip1'\1/' $file && echo "$(date) $name [${ip2}] -> [${ip1}] updated!"
		#old ip not exist: add new record
		[ "1$ip2" = "1" ] && echo "$ip1 $name" >> $file && echo "$(date) new record $name:[${ip1}] added!"
	fi
done

