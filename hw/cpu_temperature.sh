#!/bin/bash

#file: cpu_temperature.sh
#intro: display cpu temperature in Celsius degree
#usage: 
#
#
#by dolf
#2019.8.18

#date
#echo -e "\n-------------------------------------------"
d=$(date +"%Y-%m-%d %H:%M:%S")

#path of the shell
basepath=$(cd `dirname $0`;pwd)
#echo "basepath:$basepath"

#test empty string
#str=
#[ $str ] || echo \$str is null

t=$(cat /sys/class/thermal/thermal_zone0/temp | awk '{print $1/1000}')
echo [${d}]CPU temperature: ${t} Celsius degree
